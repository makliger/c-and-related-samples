//
//  list.c
//  shows various locks/threads performance
//
//  Created by Michael Kliger on 5/8/18.
//  Copyright © 2018 Michael Kliger. All rights reserved.
//

#include <stdio.h>
#include <sys/wait.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <getopt.h>
#include <errno.h>
#include <sys/poll.h>
#include <signal.h>
#include <time.h>
#include <pthread.h>
#include "SortedList.h"
#include <sched.h>



long long n_iterations = 1;
long long time_elapsed = 0;
long long n_threads = 1;
unsigned long n_lists = 1;
SortedList_t* list; // array of list heads
SortedListElement_t *elements; // array of elemen
pthread_mutex_t *locks;
pthread_t * thread;
int opt_yield = 0;
int sync_f = 0;
int  * spin_lock;
int lock_type = 0; // 0 for none, 1 for mutex, 2 for spin



void* (*list_wrapper)(void *);


/*
 
 helper
 
 functions
 
 */
void cleanup()
{
    if(list)
    free(list);
    if(elements)
    free(elements);
    if(locks)
    free(locks);
    if(thread)
    free(thread);
    if(spin_lock)
    free(spin_lock);
}
void smart_error()
{
    fprintf(stderr, "Error, see :%s  \n", strerror(errno));
    cleanup();
    exit(1);
}


unsigned long hash(unsigned char *str) // DJB hash function... source : Dan Bernstein
{
    unsigned long hash = 5381;
    int c;
    
    while (c = *str++)
        hash = ((hash << 5) + hash) + c; /* hash * 33 + c */
    
    return hash;
}

void handler(int n)
{
  if (n == SIGSEGV)
    {
      fprintf(stderr, "Seg fault caught: error = %d, message =  %s \n", n, strerror(n));
      cleanup();
      exit(2);
    }
}

long long  t_pthread_mutex_lock(pthread_mutex_t *lock)
{
    struct timespec start_t;
    if(clock_gettime(CLOCK_MONOTONIC, &start_t) < 0)
        smart_error();
    pthread_mutex_lock(lock);
    return ((start_t.tv_sec  * 1000000000L) + start_t.tv_nsec);
}
long long t_pthread_mutex_unlock(pthread_mutex_t * lock)
{
    struct timespec start_t;
    if(clock_gettime(CLOCK_MONOTONIC, &start_t) < 0)
        smart_error();
    pthread_mutex_unlock(lock);
    
    return ((start_t.tv_sec  * 1000000000L) + start_t.tv_nsec);
    
    
}


void *list_normal(void *start)
{
    int lock_num=0;
    SortedListElement_t *begin = start;
    int i;
    for(i = 0; i < n_iterations; i++)
    {
        lock_num = (int) hash(begin[i].key) % n_lists;
        SortedList_insert(list+lock_num, begin + i);
    }
    
    int len = 0;
    for(i = 0; i < (int)n_lists; i++)
    {
      int temp =  SortedList_length(list+i);
        if(temp == -1)
        {
            fprintf(stderr, "error: SortedList_length implies list is corrupted\n");
            cleanup();
            exit(2);
        }
        len += temp;
    }
    for(i = 0; i < n_iterations; i++)
    {
       lock_num = (int) hash(begin[i].key)% n_lists;
        SortedListElement_t* find = SortedList_lookup(list+lock_num, begin[i].key);
        if(find == NULL)
        {
	  fprintf(stderr, "error: SortedList_lookup failed on a known key. \n");
            cleanup();
            exit(2);
        }
        int j = SortedList_delete(find);
        if(j != 0)
        {
            fprintf(stderr, "error: delete failed \n");
            cleanup();
            exit(2);
        }
    }
    return NULL;
    
}

void *list_spin(void *start)
{
    struct timespec start_t, end_t;
    int lock_num = 0;
    
    SortedListElement_t *head = start;
    int i;
    // Insertion
    for(i = 0; i < n_iterations; i++)
    {
        
        if(clock_gettime(CLOCK_MONOTONIC, &start_t) < 0)
            smart_error();
        lock_num = (int) hash(head[i].key)% n_lists;
        while(__sync_lock_test_and_set(spin_lock+lock_num, 1))
            ;
        SortedList_insert(list+lock_num, head + i);
        __sync_lock_release(spin_lock+lock_num);
        
        if(clock_gettime(CLOCK_MONOTONIC, &end_t) < 0)
            smart_error();
        
        time_elapsed += (end_t.tv_sec - start_t.tv_sec) * 1000000000L + (end_t.tv_nsec - start_t.tv_nsec);
    }
    // Length
    int len = 0;
    for(i = 0; i < (int)n_lists; i++)
    {
        if(clock_gettime(CLOCK_MONOTONIC, &start_t) < 0)
             smart_error();
        while(__sync_lock_test_and_set(spin_lock+i, 1))
            ;
        int temp = 0;
        temp += SortedList_length(list+i);
        if(temp == -1)
        {
            fprintf(stderr,  "error:SortedList_length failed. \n");
            cleanup();
            exit(2);
        }
        len+=temp;
        __sync_lock_release(spin_lock+i);
        if(clock_gettime(CLOCK_MONOTONIC, &end_t) < 0)
            smart_error();
        time_elapsed += (end_t.tv_sec - start_t.tv_sec) * 1000000000L + (end_t.tv_nsec - start_t.tv_nsec);
    }
    
    // Delete and lookup
    for(i = 0; i < n_iterations; i++)
    {
        if(clock_gettime(CLOCK_MONOTONIC, &start_t) < 0)
            smart_error();
        lock_num = (int) hash(head[i].key)% n_lists;
        while(__sync_lock_test_and_set(spin_lock+lock_num, 1))
            ;
        SortedListElement_t* find = SortedList_lookup(list+lock_num, head[i].key);
        if(find == NULL)
        {
            fprintf(stderr,  "error:SortedList_lookup failed on known key. \n");
            cleanup();
            exit(2);
        }
        int j = SortedList_delete(find);
        if(j != 0)
        {
            fprintf(stderr, "error: delete() failed. \n");
            cleanup();
            exit(2);
        }
        __sync_lock_release(spin_lock+lock_num);
        if(clock_gettime(CLOCK_MONOTONIC, &end_t) < 0)
            smart_error();
        time_elapsed += (end_t.tv_sec - start_t.tv_sec) * 1000000000L + (end_t.tv_nsec - start_t.tv_nsec);
    }
   
    
    return NULL;
    
}


void *list_mutex(void *argz)
{
    int lock_num = 0;
    SortedListElement_t *begin =argz;
    int i;
    long long start;
    for(i = 0; i < n_iterations; i++)
    {
        lock_num = (int)hash(begin[i].key)% n_lists;
        long long start = t_pthread_mutex_lock(locks+lock_num);
        SortedList_insert(list+lock_num, begin + i);
        time_elapsed += (t_pthread_mutex_unlock(locks+lock_num) - start);
       
    }
    int len = 0;
    for(i = 0; i < (int)n_lists; i++)
    {
     start = t_pthread_mutex_lock(locks+i);
     int temp  = SortedList_length(list+i);
     if(temp == -1)
        {
            fprintf(stderr, "error: SortedList_length implies list is corrupted\n");
            cleanup();
            exit(2);
        }
     len += temp;
     time_elapsed += (t_pthread_mutex_unlock(locks+i) - start);
    }
    
    for(i = 0; i < n_iterations; i++)
    {
        lock_num = (int) hash(begin[i].key)% n_lists;
        start = t_pthread_mutex_lock(locks+lock_num);
       SortedListElement_t* find = SortedList_lookup(list+lock_num, begin[i].key);
        if(find == NULL)
        {
	  fprintf(stderr, "error: SortedList_lookup failed on known key. \n");
            cleanup();
            exit(2);
        }

        int j = SortedList_delete(find);
        if(j != 0)
        {
	  
	  fprintf(stderr, "delete() failed \n");
            cleanup();
            exit(2);
        }
        time_elapsed += (t_pthread_mutex_unlock(locks+lock_num) - start);

       
    }
    
    return NULL;
    
}



int main(int argc, char ** argv)

{
  
     int c;
    while (1)
    {
        static struct option long_options[] =
        {
            {"iterations", required_argument, 0, 'i'},
            {"threads", required_argument, 0, 't'},
            {"yield", required_argument,0, 'y'},
            {"sync", required_argument, 0, 's'},
            {"lists", required_argument, 0, 'l'},
            {0,         0,                 0,  0 }
        };
        c = getopt_long(argc, argv, "",
                        long_options, NULL);
        if (c== -1)
            break;
        else if (c == 'i')
        {
            n_iterations = atoi(optarg);
        }
        else if (c == 't')
        {
            n_threads = atoi(optarg);
        }
        else if (c == 'l')
        {
            n_lists = atoi(optarg);
        }
        else if (c=='y')
        {
	 char *  yield_s = malloc(strlen(optarg)+1);
	  strcpy(yield_s, optarg);
            if(strlen(yield_s) > 3 )
            {
	        free(yield_s);
                printf("unrecognized yield options see usage \n");
                exit(1);
            }
            
            int ii = 0;
            int dd = 0;
            int ll = 0;
            char * iii = strchr(yield_s, 'i');
            char * lll = strchr(yield_s, 'l');
            char * ddd = strchr(yield_s, 'd');
            
            if(iii != NULL)
            {
                ii = 1;
                opt_yield |= INSERT_YIELD;
            }
            if(lll != NULL)
            {
                ll = 1;
                opt_yield |= LOOKUP_YIELD;
            }
            if(ddd != NULL)
            {
               dd = 1;
               opt_yield |= DELETE_YIELD;
            }
            
               
            
            
            if((size_t)(dd + ll + ii) != strlen(yield_s))
            {
                printf("improper yield options see usage \n");
                free(yield_s);
		exit(1);
            }
        
            free(yield_s);
        }
        else if (c == '?')
        {
            printf("unrecognized options see usage \n");
            exit(1);
        }
        else if (c =='s')
        {
            if(strcmp("m", optarg) == 0)
                lock_type = 1;
            else if(strcmp("s", optarg) == 0)
               lock_type = 2;
            else
                {
                    printf("unrecognized options see usage \n");
                    exit(1);
                }
       
        }
        else
            continue;
    }
    
    //init LL
    signal(SIGSEGV, handler);
    
    int i;

    list = malloc(sizeof(SortedList_t) *n_lists); // each head
    list_wrapper = lock_type == 0 ? list_normal : lock_type == 1 ? list_mutex : list_spin;
    locks = malloc(sizeof(pthread_mutex_t) * n_lists);
    spin_lock = malloc(sizeof(int) * n_lists);
    for(i =0; i< (int)n_lists;i++)
    {
        (list+i)->key = NULL;
        (list+i)->next = (list+i)->prev = (list+i);
        pthread_mutex_init(locks+i, NULL);
        *(spin_lock + i) = 0;
    }
    SortedListElement_t *elements = malloc(n_threads*n_iterations * sizeof(SortedListElement_t));
    
    // create keys
    
    srand(time(NULL));
    
    for(i=0;i< n_threads * n_iterations ;i++)
    {
        int size = rand() % 15 + 1;
        int a_offset = rand() % 26;
        char * key = malloc((size+1) * sizeof(char));
        int j;
        for(j=0 ; j < size; j++)
        {
            key[j] = 'a' + a_offset;
            a_offset = rand() % 26;
        }
        key[size] = '\0'; 
        elements[i].key = (const char *) key;
    }
    
    // create threads
    i = 0;
    long long n_op = n_iterations * 3 * n_threads;
    thread = malloc(sizeof(pthread_t)*n_threads);
    struct timespec start_t, end_t;
    
   
    if(clock_gettime(CLOCK_MONOTONIC, &start_t) < 0)
        smart_error();
    
    for(i=0; i < n_threads; i++)
    {
        int ret = pthread_create(thread + i, NULL, list_wrapper, elements + i*n_iterations);
        if(ret)
            smart_error();
    }
     for(i=0; i < n_threads; i++)
    {
        int rs = pthread_join(thread[i], NULL);
        if(rs)
            smart_error();
    }
    if(clock_gettime(CLOCK_MONOTONIC, &end_t) < 0)
        smart_error();
    
    
    long long nanoseconds = (end_t.tv_sec - start_t.tv_sec) * 1000000000L + (end_t.tv_nsec - start_t.tv_nsec);
    
    if(SortedList_length(list) != 0)
    {
        fprintf(stderr, "corrupted list: length != 0\n");
        cleanup();
        exit(2);
    }
    

    
    
    
    
    
    
    char *yield_str;
    if(opt_yield == 0)
        yield_str = "none";
    else if((opt_yield & INSERT_YIELD) && (opt_yield & DELETE_YIELD) && (opt_yield & LOOKUP_YIELD))
        yield_str = "idl";
    else if((opt_yield & INSERT_YIELD) && (opt_yield & DELETE_YIELD))
        yield_str = "id";
    else if((opt_yield & INSERT_YIELD) && (opt_yield & LOOKUP_YIELD))
        yield_str = "il";
    else if(opt_yield & INSERT_YIELD)
        yield_str = "i";
    else if((opt_yield & DELETE_YIELD) && (opt_yield & LOOKUP_YIELD))
        yield_str = "dl";
    else if(opt_yield & DELETE_YIELD)
        yield_str = "d";
    else
        yield_str = "l";
    
    long long avg = nanoseconds/n_op;
    long long lock_ops = n_threads * (3 * n_iterations + 1); //to account for loc
    long long lock_avg = time_elapsed / lock_ops;
    
    printf("list-%s-%s,%lld,%lld,%d,%lld,%lld,%lld,%lld\n", yield_str,
           lock_type == 0 ? "none" : lock_type == 1 ? "m" : "s",
           n_threads, n_iterations, n_lists, n_op, nanoseconds, avg, lock_avg);
    
    
    
    cleanup();
    exit(0);
    
    

}
