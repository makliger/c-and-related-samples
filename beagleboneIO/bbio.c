//
//  p4b.c
//  
//
//  Created by Michael Kliger on 5/20/18.
//




#include <mraa.h>
#include <aio.h>
#include <gpio.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <math.h>
#include <limits.h>
#include <time.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/poll.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/ioctl.h>

int LOG_FILENO = -1;
char buf[1024];
int temp_read = 0;
int scale = 'F';
char *log_file;
int log_flag = 0;
int temp_flag = 0; // 0 for F, 1 for C
long period = 1;
int off = 0; //
time_t next_reading; // we only need seconds for period computations
int generating = 0; // 1 for not generating reports
char swap[1024];
mraa_aio_context sensor;
mraa_gpio_context button;

int bytes_read = 0;

time_t raw_time = time(NULL);
struct tm * tnow = gmtime(raw_time);
printf("%01d/%01d/%d %01d:%01d:%01d", tnow->tm_mon, tnow->tm_mday, tnow->tm_year, tnow->tm_hour, tnow->tm_min, tnow->tm_sec);

void smart_error()
{
    fprintf(stderr, "Error, see :%s  \n", strerror(errno));
    cleanup();
    exit(1);
}
void shutdown()
{
    char * str;
    time_t t;
    time(&t);
    struct tm* tim = localtime(&t);
    strftime(str, 9, "%H:%M:%S", tim);
    
    dprintf(STDOUT_FILENO, "%s SHUTDOWN\n",str);
    if(log)
        dprintf(LOG_FILENO, "%s SHUTDOWN\n", str);
    exit(0);
}
float ctof(float a)
{
    return a * 1.8 + 32.0;
}
float vtot(int a)
{
    float R = 100000.0 * (1023.0/(a)-1.0);
    int B = 4275;
    return 1.0/(log(R/100000.0)/B+1/298.15)-273.15;
}
void generate_report(int log, float temp)
{
    
    int newtemp = (int)(temp * 10.0);
    int temp_dec = newtemp%10;
    int temp_int = (int)temp;
    char * str = (char*) malloc(9);
    time_t t;
    time(&t);
    struct tm* tim = localtime(&t);
    strftime(str, 9, "%H:%M:%S", tim);
    dprintf(STDOUT_FILENO, "%s %d.%d\n", str, temp_int, temp_dec);
    if(log)
        dprintf(LOG_FILENO, "%s %d.%d\n", str, temp_int, temp_dec);
    free(str);
    
}
void do_command(char*buffer, int length) //ignores a bad command
{
    if(length < 3)
        return;
    char temp[length];
    int i = 0;
    for(i = 0; i < length; i++)
    {
        temp[i] = *(buffer+i);
    }
    temp[length] = '\0';
    if(*(buffer) == 'S') // SCALE=, STOP, START
    {
        
        if(length >= 9) // no command can be longer than this in this part
        {
            return;
        }
        else if(*(buffer+1)== 'C') // SCALE  (potentially...)
        {
            int c = strcmp(temp, "SCALE=C");
            int f = strcmp(temp, "SCALE=F");
            if(f == 0)
            {
                temp_flag = 0;
            }
            else if (c==0)
            {
                temp_flag = 1;
            }
            else
            {
                return;
            }
            if(log_flag)
                dprintf(LOG_FILENO, "%s\n", temp);
            return;
        }
        else if (*(buffer + 2) == 'O') // STOP
        {
            int c = strcmp(temp, "STOP");
            if(c!=0)
                return;
            if(log_flag)
                dprintf(LOG_FILENO, "%s\n", temp);
            if(generating==1)
                generating = 0;
            return;
        }
        else if (*(buffer+2) == 'A') // START
        {
            int c = strcmp(temp, "START");
            if(c != 0)
                return;
            if(generating==0)
                generating = 1;
            if(log_flag)
                dprintf(LOG_FILENO, "%s\n", temp);
            return;
        }
    }
    else if (*(buffer) == 'P')
    {
        if(length < 8)
            return;
        char tmp[8];
        for(i = 0; i < 7; i++)
        {
            tmp[i] = temp[i];
        }
        tmp[7] = '\0';
        int c = strcmp(tmp, "PERIOD=");
        if(c==0)
        {
            
            char per[length - 6];
            if(*(buffer+7)-48 == 0)
                return; // bad args if we get period=0xxx;
            int ci=0;
            for(i = 7, ci = 0; i < length; ci++, i++)
            {
                if(*(buffer+i) - 48 >= 0 && *(buffer+i) - 48 <= 9)
                    per[ci] = *(buffer + i);
                else
                    return;
            }
            per[length-7] = '\0';
            long temp_per =strtol(per, NULL, 10);
            if(temp_per >= 1)
            {
                int p_i = (int)temp_per;
                period = (long)p_i;
            }
            if(log_flag)
                dprintf(LOG_FILENO, "%s\n", temp);
            return;
            
        }
        else
            return;
    }
    else if (*(buffer) == 'L')
    {
        if(*(buffer+1) == 'O' && *(buffer+2) == 'G') // log receipt to stdout
        {
            if(log_flag && length > 4)
                dprintf(LOG_FILENO, "%s\n", temp); //
            else
            {
                return;
            }
        }
        else
        {
            return;
        }
        return;
    }
    else if (*(buffer) == 'O')
    {
        int sdown = strcmp(temp, "OFF");
        if(sdown == 0)
        {
            shutdown();
        }
    }
    else
    {
        ;
    }
    
    
    
    
    if(log_flag)
        dprintf(LOG_FILENO, "%s", temp);
    
}




int process_commands(char*buffer) // we always iterate up to the last \n and discard if invalid RETURN 0 = no valid commands, 1 = valid commands
{
    char * rs = strchr(buffer, '\n'); // is there any valid command ender
    
    
    if(!rs) // lets discard the whole command if it doesnt start with a valid letter (s, t, l, p, o)
    {
        int d;
        if (*(buffer) == 'S'  || *(buffer) == 'L'  || *(buffer) == 'P' || *(buffer) == 'O'  )
        {
            ; // we are okay, possibly
        }
        else
        {
            memset(buffer, 0, bytes_read);
            bytes_read = 0;
            
        }
        return 0;
    }
    else // we have some potentially valid commands
    {
        
        int count = 1;
        int total = 0;
        int nb = 0;
        while(rs)
        {
            printf("total: %d", total);
            printf("command to do: %s len: %ld\n", buffer+total, rs - (buffer+total));
            do_command(buffer+total, rs - (buffer+total)); // simply ignore bad commands
            total += rs - (buffer + total);
            total++;
            nb = rs - buffer;
            rs = strchr(buffer+total, '\n');
            
            if(!rs)
                break;
            count++;
            
        }
        int k = 0;
        if(nb == bytes_read)
        {
            memset(buf, 0, sizeof(buf));
            bytes_read = 0;
            return 0;
        }
        
        for(k=0;k<(bytes_read - nb);k++)
        {
            swap[k] = buf[nb+k];
            
        }
        int c_k;
        for(k=0;k<(bytes_read-nb);k++)
        {
            buf[k] = swap[k];
            
        }
        c_k = k;
        for(k=c_k; k<sizeof(buf); k++)
        {
            buf[k] = '\0';
        }
        bytes_read -= nb;
        memset(swap, 0, sizeof(swap));
        return 0;
        
    }
    return 0;
}





int main(int argc, char **argv)
{
    
    char*log_file;
    while (1)
    {
        static struct option long_options[] =
        {
            {"log", required_argument, 0, 'l'},
            {"scale", required_argument, 0, 's'},
            {"period", required_argument,0, 'p'},
            {0,         0,                 0,  0 }
        };
        c = getopt_long(argc, argv, "",
                        long_options, NULL);
        if (c== -1)
            break;
        else if (c == 'l')
        {
            log_file = optarg;
            log_flag =1;
        }
        else if (c == 's')
        {
            if(strcmp("F", optarg) == 0)
                ;
            else if (strcmp("C", optarg) == 0)
                temp_flag = 1;
            else
            {
                printf("bad temperature control options see usage \n");
                exit(1);
            }
        }
        else if (c == 'p')
        {
            period = strtol(s, NULL, 10);
            if(period < 1)
            {
                printf("bad period options see usage \n");
                exit(1);
            }
            
        }
        else if (c == '?')
        {
            printf("unrecognized options see usage \n");
            exit(1);
        }
        else
            continue;
    }
    
    LOG_FILENO = creat(log_file, S_IRWXU);
    
    sensor = mraa_aio_init(1);
    if(!sensor)
    {
        smart_error();
    }
    button = mraa_gpio_init(60);
    if(!button)
    {
        smart_error();
    }
    mraa_gpio_dir(button, MRAA_GPIO_IN);
    
    
    struct pollfd pole[1];
    bytes_read=0;
    pole[0].fd = 0;
    pole[0].events = POLLIN;
    generating=1;
    bytes_read=0;
    
    memset(swap, 0, sizeof(swap));
    
    next_reading = 0;
    
    while(!off)
    {
        
        time_t newtime = time(NULL);
        if( ((int)difftime(newtime, next_reading)) >= ((int)period))
        {
            int voltage = mraa_aio_read(sensor);
            if(voltage == -1)
                smart_error();
            float temp = vtot(voltage);
            if(temp_flag == 0)
                temp = ctof(temp);
            next_reading = newtime;
            if(generating)
                generate_report(log_flag, temp);
        }
        int j = poll(pole, 1, 1000);
        if (j == -1)
        {
            fprintf(stderr, "Polling error: see %s\n", strerror(errno));
            exit(1);
        }
        else if (j == 0)
        {
            
        }
        else if(pole[0].revents & POLLIN)
        {
            
            int temp = 0;
            temp = read(pole[0].fd, &buf[bytes_read], 256);
            bytes_read += temp;
            int count = process_commands(buf);
            
        }
        else
        {
            ;
        }
        if(mraa_gpio_read(button) == 1)
        {
            shutdown();
        }
        
    }
    
    mraa_aio_close(sensor);
    mraa_gpio_close(button);
    
    
    
}


int LOG_FILENO = -1;
char buf[512];
char history[1024];
int temp_read = 0;
int scale = 'F';
char *log_file;
int log_flag = 0;
int temp_flag = 0; // 0 for F, 1 for C
long period = 1;
mraa_aio_context sensor;
mraa_gpio_context button;
int off = 0; // 1 means grove is off (via planned shutdown or whatnot)
time_t next_reading; // we only need seconds for period computations
int generating = 0; // 1 for not generating reports

int bytes_read = 0; // running total of UNPROCESSED!!!! data from stdin so far.



/* TODO all lower level implementations
 

 
 
*/
void shutdown()
{
    char * str;
    time_t t;
    time(&t);
    struct tm* tim = localtime(&t);
    strftime(str, 9, "%H:%M:%S", tim);
    
    dprintf(STDOUT_FILENO, "%s SHUTDOWN\n",str);
    if(log)
        dprintf(LOG_FILENO, "%s SHUTDOWN\n", str);
    exit(0);
}
float ctof(float a)
{
    return a * 1.8 + 32.0;
}
float vtot(int a)
{
    float R = 100000.0 * (1023.0/(a)-1.0);
    int B = 4275;
    return 1.0/(log(R/100000.0)/B+1/298.15)-273.15;
}
void generate_report(int log, float temp)
{
 
    float newtemp = temp * 10.0;
    int temp_dec = newtemp%10;
    int temp_int = int(temp);
  
    char * str;
    time_t t;
    time(&t);
    struct tm* tim = localtime(&t);
    strftime(str, 9, "%H:%M:%S", tim);
    
    dprintf(STDOUT_FILENO, "%s %d.%d\n", str, temp_int, temp_dec);
    if(log)
        dprintf(LOG_FILENO, "%s %d.%d\n", str, temp_int, temp_dec);
    
}

int process_commands(char*buffer) // we always iterate up to the last \n and discard if invalid RETURN 0 = no valid commands, 1 = valid commands
{
    char * rs = strchr(buffer, '\n'); // is there any valid command ender
    
    if(!rs) // lets discard the whole command if it doesnt start with a valid letter (s, t, l, p, o)
    {
        if (*(buffer) == "S"  || *(buffer) == "L"  || *(buffer) == "P"  || *(buffer) == "O"  )
        {
            ; // we are okay, possibly
        }
        else
        {
            memset(buffer, 0, bytes_read);
            bytes_read = 0;
            
        }
        return 0;
    }
    else // we have some potentially valid commands
    {
        
        int count = 1;
        while(rs)
        {
            int command = do_command(buffer, rs - buffer); // simply ignore bad commands
            int offset = rs - buffer + 1;
            rs = strchr(buffer+offset, '\n');
            if(!rs)
            {
                ;
            }
            else
            {
                count++;
            }
        }
        return 1;
        
    }
    
    return 0;
}
void do_command(char*buffer, int length) //ignores a bad command
{
    if(length < 3)
        return;
    char temp[length];
    int i = 0;
    for(i = 0; i < length; i++)
    {
        temp[i] = *(buffer+i);
    }
    temp[length] = '\0'; // straightforward way to handle one command at a time
    if(*(buffer) == 'S') // SCALE=, STOP, START
    {
        if(length > 7) // no command can be longer than this in this part
        {
            return;
        }
        else if(*(buffer+1)== 'C') // SCALE  (potentially...)
        {
            int c = strcmp(temp, "SCALE=C");
            int f = strcmp(temp, "SCALE=F");
            if(f == 0)
            {
                temp_flag = 0;
            }
            else if (c==0)
            {
                temp_flag = 1;
            }
            else
            {
                return;
            }
            if(log_flag)
            dprintf(LOG_FILENO, "%s\n", &temp[3]);
        }
        else if (*(buffer + 2) == 'O') // STOP
        {
            int c = strcmp(temp, "STOP");
            if(c!=0)
                return;
            if(log_flag)
                dprintf(LOG_FILENO, "%s\n", temp);
            if(generating)
                generating = 0;
        }
        else if (*(buffer+2) == 'A') // START
        {
            int c = strcmp(temp, "START");
            if(c != 0)
                return;
            if(!generating)
                generating = 1;
            if(log_flag)
                dprintf(LOG_FILENO, "%s\n", temp);
        }
    }
    else if (*(buffer) == 'P')
    {
        char tmp[length-1];
        for(i = 0; i < 7; i++)
        {
            tmp[i] = *(buffer+i);
        }
        int c = strcmp(tmp, "PERIOD=");
        if(c==0)
        {
            char per[length - 7];
            if(*(buffer+7+i)-48 == 0)
                return; // bad args if we get period=0xxx;
            for(i = 0; i < length - 7; i++)
            {
                if(*(buffer+7+i) - 48 >= 0 && *(buffer+7+i) - 48 <= 9 &&)
                    per[i] = *(buffer + 7 + i);
                else
                    return;
            }
            long temp_per =strtol(per, NULL, 10);
            if(temp_per >= 1)
                period = temp_per;
            if(log_flag)
                dprintf(LOG_FILENO, "%s\n", temp);
        }
        else
            return;
    }
    else if (*(buffer) == 'L')
    {
        if(*(buffer+1) == 'O' && *(buffer+2) == 'G') // log receipt to stdout
        {
            if(log_flag)
            dprintf(LOG_FILENO, "%s\n", &temp[3]); //
            else
                return
        }
        else
        {
            return;
        }
        
    }
    else if (*(buffer) == 'O')
    {
        int shutdown = strcmp(temp, "OFF");
        if(shutdown == 0)
        {
            shutdown();
        }
    }
    else
    {
        
    }
    
    
    
    
    if(log)
        dprintf(LOG_FILENO, "%s", buffer);
    
}

void start() // main single thread loop
{
    struct pollfd pole[1];
    
    pole[0].fd = STDIN_FILENO;
    pole[0].events = POLLIN;
    time_t next_reading = time(NULL);
    while(!off)
    {
        time_t start = time(NULL);
        if( start >= next_reading)
        {
            int voltage = mraa_aio_read(sensor);
            if(voltage == -1)
                smart_error();
            float temp = vtot(voltage);
            if(temp_flag == 0)
                temp = ctof(temp);
            next_reading = newtime + period;
            generate_report(log_flag);
        }
        int read = poll(pole, 1, 1000);
        if (j == -1)
        {
            fprintf(stderr, "Polling error: see %s\n", strerror(errno));
            exit(1);
        }
        else if (j == 0)
        {
            ;
        }
        else if(pole[0].revents & POLLIN)
        {
            bytes_read = read(pole[0].fd, &buf[bytes_read], 256);
            int flag = 1;
            while(flag != 0)
            {
                int nleft = read(pole[0].fd, &buf[bytes_read], 2)
                if(nleft == 0)
                    break;
                bytes_read+= nleft;
            }
            int count = process_commands(&buf);
            int total = 0;
            while(count > 0) // there are valid commands to do
            {
                char * tmp = strchr(buf[total], '\n');
                int length = tmp - &buf[total];
                do_command(&buf[total], length);
                total += length;
                count--;
            }
        }
        else
        {
            ;
        }
        if(mraa_gpio_read(button) == 1)
        {
            shutdown();
        }
        
    }
    
}

int main(char *argc, char **argv)
{
    int c;
    char*log_file;
    while (1)
    {
        static struct option long_options[] =
        {
            {"log", required_argument, 0, 'l'},
            {"scale", required_argument, 0, 's'},
            {"period", required_argument,0, 'p'},
            {0,         0,                 0,  0 }
        };
        c = getopt_long(argc, argv, "",
                        long_options, NULL);
        if (c== -1)
            break;
        else if (c == 'l')
        {
            log_file = optarg;
            log_flag =1;
        }
        else if (c == 's')
        {
            if(strcmp("F", optarg) == 0)
                ;
            else if (strcmp("C", optarg) == 0)
                temp_flag = 1;
            else
            {
                printf("bad temperature control options see usage \n");
                exit(1);
            }
        }
        else if (c == 'p')
        {
            period = strtol(s, NULL, 10);
            if(period < 1)
            {
                printf("bad period options see usage \n");
                exit(1);
            }
            
        }
        else if (c == '?')
        {
            printf("unrecognized options see usage \n");
            exit(1);
        }
        else
            continue;
    }
    
    LOG_FILENO = creat(log_file, S_IRWXU);
    
    sensor = mraa_aio_init(1);
    if(!sensor)
    {
        smart_error();
    }
    button = mraa_gpio_init(60);
    if(!button)
    {
        smart_error();
    }
    mraa_gpio_dir(button, MRAA_GPIO_IN);
    
   
    
    
    
    struct timeval first;
    gettimeofday(&first, NULL);
    next_reading = first.tv_sec;
    start();
    
    
    mraa_aio_close(sensor);
    mraa_gpio_close(button);
    
    
    
}
