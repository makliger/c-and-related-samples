// Michael Kliger

#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include "zlib.h"
#include <sys/wait.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <termios.h>
#include <unistd.h>
#include <sys/types.h>
#include <getopt.h>
#include <errno.h>
#include <sys/poll.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/ioctl.h>

#define CHUNK 16384
//globals (most shouldnt be global, but i was lazy)
struct termios t_restore;
int shellFlag = 0;
pid_t cpid;
int debugFlag = 0;
char buf[256];
int pipets[2];
int pipefs[2];

char output[5000];

z_stream strm;
z_stream strm_client;


int portno;
int compressFlag = 0;
int newsockfd;
// helper functions


void smart_error(char * message)
{
  fprintf(stderr, "Error in usage of %s, see : %s \n", message, strerror(errno));
    exit(1);
}
void close_proccesses()
{
    int j;
    
    if(shellFlag == 0)
        ;
    else
    {
        pid_t shell_wait = waitpid(cpid, &j, 0);
        if(shell_wait == -1)
        {
            fprintf(stderr, "Error with waitpid, see: %s\n", strerror(errno));
        }
        fprintf(stderr, "SHELL EXIT SIGNAL=%d STATUS=%d\n", WTERMSIG(shell_wait), WEXITSTATUS(shell_wait));
        // wait for cleanup of shell
    }
    
    
    
}

void dup2_with_error(int oldfd, int newfd)
{
    int j = dup2(oldfd, newfd);
    if(j == -1)
    {
        fprintf(stderr, "Error using dup2, see: %s\n",strerror(errno) );
        exit(1);
    }
}
int  write_with_error(int fd, char* buffer, ssize_t bytes)
{
    ssize_t nb = write(fd, buffer, bytes);
    if(nb < 0)
    {
        fprintf(stderr, "unable to perform write. See : %s .\n", strerror(errno));
        exit(1);
    }
    return nb;
}
int read_with_error(int fd, char* buffer, ssize_t bytes)
{
    ssize_t nb = read(fd, buffer, bytes);
    if(nb < 0)
    {
        fprintf(stderr, "unable to perform read. Exiting.\n");
        exit(1);
    }
    return nb;
}

void restore_terminal()
{
    
    close_proccesses();
    if(debugFlag == 1)
    {
        printf("Terminal Restoration Process. \n");
    }
}

void close_with_error(int fd)
{
    int i = close(fd);
    if(i < 0)
    {
        fprintf(stderr, "Close failed with: %s\n",strerror(errno));
        exit(1);
    }
    
}
int Xcompress (char * buffer, z_stream strm_client)
{

strm_client->zalloc = Z_NULL;
strm_client->zfree = Z_NULL;
strm_client->opaque = Z_NULL;

strm_client->avail_in = strlen(buffer)+1;
strm_client->next_in = buffer;
strm_client->avail_out = CHUNK;
strm_client->next_out = output;

deflateInit(strm_client, Z_DEFAULT_COMPRESSION);
deflate(strm_client, Z_SYNC_FLUSH);
deflateEnd(strm_client);

return strm->total_out;
}
int Xdecompress( char * buffer, z_stream &strm)
{

strm->zalloc = Z_NULL;
strm->zfree = Z_NULL;
strm->opaque = Z_NULL;

strm->avail_in = bytes;
strm->next_in = buffer;
strm->avail_out = CHUNK;
strm->next_out = output;

inflateInit(strm);
inflate(strm, Z_SYNC_FLUSH);
inflateEnd(strm);

return strm->total_out;
}

// all i/o processing
void process_input(char * buffer, ssize_t bytes, int fd)
{
    int i =0;
    if(compressFlag == 1 && fd == pipets[1])
    {
decompress(&buffer, &strm);

    }
    else if (compressFlag == 1 && fd != pipets[1])
    {
compress(&buffer, &strm);
    }
    else
    {
        ;
    }
    for(i = 0; i < bytes; i++)
    {
if(compressFlag && fd == pipets[1])
        {
            switch(*(output+i))
        
                {
                        case 0x04:
                        {
                            close(pipets[1]);
                            break;
                    
                        }
                    case 0x03:
                        {
                            kill(cpid, SIGINT);
                            break;
                        }
                    default:
                        write_with_error(fd, output+i, 1);
                
            }
        }
        else
        {
            switch(*(buffer))
            
            {
                case 0x04:
                {
                    close(pipets[1]);
                    break;
                    
                }
                case 0x03:
                {
                    if(fd == pipets[1])
                        kill(cpid, SIGINT);
                    else
                        write_with_error(fd, buffer+i, 1);

                    break;
                }
                default:
                    write_with_error(fd, buffer+i, 1);
                    
            }
        }
    }
    
    memset(&strm, 0, sizeof strm);
    memset(&buffer, 0, sizeof buffer);
    memset(&output, 0, sizeof output);
    memset(&strm_client, 0, sizeof strm_client);
    
}

void handler(int signal)
{
   
    if(signal == SIGPIPE)
    {
        if(debugFlag == 1)
            printf("got sigpipe \n.");
        
        exit(0);
    }
}

void run_shell(int socket_descriptor)
{	//**********************************************
    // shell implementation notes and goals:
    
    // pipe before fork to preserve file descriptors
    
    // two pipes will be created: toshell (ts) fromshell (fs)
    
    //Parent side:
    // write input to shell (close read end of ts)
    // read from shell to output (close write end of fs)
    
    //Child Side:
    // read input from parent (terminal) close read end of ts
    // write output to parent (terminal) close write end of fs
    
    // close unneeded fd's
    
    
    //**********************************************
    if(pipe(pipets) == -1)
    {
         smart_error("pipe call 1");
    }
    if(pipe(pipefs) == -1){
        smart_error("pipe call 2");
    }
    cpid = fork();
    if (cpid == -1)
    {
         smart_error("fork");
    }
    else if (cpid == 0) // we know we are in child process
    {
        close_with_error(pipets[1]);
        close_with_error(pipefs[0]);
        // read input from client (parent)
        
        dup2_with_error(pipets[0], 0); // map from terminal to stdin
        dup2_with_error(pipefs[1], 1); // to terminal to stdout
        dup2_with_error(pipefs[1], 2); // to terminal to stderr
        
        close_with_error(pipets[0]);
        close_with_error(pipefs[1]);
        
        // execute bash
        char * cmds[2];
        cmds[0]= "/bin/bash";
        cmds[1] = NULL;
        printf("about to exec\n");
        int o = execvp("/bin/bash", cmds);
        if(o == -1)
        {
            smart_error("exec");
        }
    }
    else    
    {
        close_with_error(pipets[0]);
        close_with_error(pipefs[1]);
        struct pollfd pole[2];
        pole[0].fd = socket_descriptor;
        pole[1].fd = pipefs[0];
        pole[0].events = POLLIN | POLLHUP | POLLERR;
        pole[1].events = POLLIN | POLLHUP | POLLERR;
        
        while(1)
        {
            int j = poll(pole, 2, 0);
            if (j == -1)
            {
                smart_error("polling");
            }
            else
            {
                if (j ==0)
                    continue;
                if(pole[0].revents & POLLIN)
                {
                    //read input from the client. forward to shell
                    char socket_in[256];
                    ssize_t bytes_read;
                    bytes_read = read_with_error(0, socket_in, 256);
		    if(bytes_read <=0)
		      {
			kill(cpid, SIGTERM);
			exit(0);
		      }
                    process_input(socket_in,  bytes_read, pipets[1]);
                }
                if(pole[1].revents & POLLIN)
                {
                    // input from shell to server
                    // output to client
                    char shell_in[256];
                    ssize_t bytes_read = read_with_error(pole[1].fd, shell_in, 256);
		    if(bytes_read <= 0)
		      {
			exit(0);
		      }
                    process_input(shell_in, bytes_read, socket_descriptor);
                }
                if(pole[1].revents & (POLLHUP | POLLERR))
                {
                    // here we use regular close so we dont have the wrong exit code
                    close(pipets[1]);
                    exit(0);
                }
                if(pole[0].revents & (POLLHUP | POLLERR))
                {
                    close(pipets[1]);
                    exit(1);
                }
            }
            
        }
        
        
        
        
        
    }
    
    
    
    
    
}





int main(int argc, char **argv)
{
    int c;
    // debugFlag = 1;
    atexit(restore_terminal);
    signal(SIGINT, handler);
    signal(SIGPIPE, handler);
    signal(SIGTERM, handler);   
    // get options
    while (1) {
        static struct option long_options[] = {
            {"compress",    no_argument, 0,  'c'},
            {"debug", no_argument, 0, 'd'},
            {"port", required_argument, 0, 'p'},
            {0,         0,                 0,  0 }
        };
        c = getopt_long(argc, argv, "",
                        long_options, NULL);
        if (c == -1)
            break;
        else if (c == 'c')
        {
           	compressFlag =1;
        }
        else if (c== 'p')
        {
            portno = atoi(optarg);
        }
       	else if( c== 'd')
       	{
            debugFlag =1;
       	}
        else if (c == '?')
        {
            fprintf(stderr, "one or more options is unrecognized . \n");
            exit(1);
        }
        else
            continue;
    }
 
    int sockfd, clilen, pid;
    struct sockaddr_in serv_addr, cli_addr;
    
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
        smart_error("socket");

 
    memset((char *)&serv_addr,0, sizeof serv_addr);
    
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(portno);
    
    if (bind(sockfd, (struct sockaddr *) &serv_addr,
             sizeof(serv_addr)) != 0)
    {
        smart_error("bind");
    }
    //printf("!!!!!!");
    if (listen(sockfd, 5) < 0)
    {
        smart_error("listen");
    }
    clilen = sizeof(cli_addr);
    newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);   
    
    printf("accepted connectino \n");
    run_shell(newsockfd);
    
    
    
    
}
