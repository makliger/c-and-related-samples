// Michael Kliger


#include <sys/wait.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <termios.h>
#include <unistd.h>
#include <sys/types.h>
#include <getopt.h>
#include <errno.h>
#include <sys/poll.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include "zlib.h"


#define CHUNK 16384
struct termios t_restore;
pid_t cpid;
int debugFlag = 0;
// these should be initialized in main rather than globally

char buf[256];
int pipets[2];
int pipefs[2];
int compressFlag = 0;
int logFlag = 0;





int ret, flush;
unsigned have;
z_stream strm;
z_stream strm_server;
unsigned char in[CHUNK];
unsigned char out[CHUNK];
char * logFile[256];






// helper functions

void smart_error(char * message)
{
  fprintf(stderr, "Error in usage of %s, see : %s \n", message, strerror(errno));
    exit(1);
}

void dup2_with_error(int oldfd, int newfd)
{
    int j = dup2(oldfd, newfd);
    if(j == -1)
    {
        fprintf(stderr, "Error using dup2, see: %s\n",strerror(errno) );
        exit(1);
    }
}
void log_with_error(int fd, char *buffer, ssize_t bytes, int s_r)
{
    if(logFlag == 0)
        return;
    int logfd;
    printf("logging\n");
    logfd = open(logFile,( O_APPEND | O_CREAT));
    if(logfd < 0)
        smart_error("open");
    
    if(s_r == 1) // sent bytes
    {
        if(dprintf(logfd, "SENT %d bytes: ", bytes)<0)
            smart_error("dprint");
        write_with_error(logfd, "\n", 1);
    }
    else
    {
        if(dprintf(logfd, "RECEIVED %d bytes: ", bytes)<0)
            smart_error("dprint");
        write_with_error(logfd, "\n", 1);
    }
   
    ssize_t lb = write(logfd, buffer, bytes);
    if(lb < 0)
        smart_error("write");
    
}
int  write_with_error(int fd, char* buffer, ssize_t bytes)
{
    
    ssize_t nb = write(fd, buffer, bytes);
    if(nb < 0)
    {
        smart_error("write");
    }
    return nb;
}
int read_with_error(int fd, char* buffer, ssize_t bytes)
{
    ssize_t nb = read(fd, buffer, bytes);
    if(nb < 0)
    {
        fprintf(stderr, "unable to perform read. Exiting.\n");
        exit(1);
    }
    return nb;
}

void restore_terminal()
{
    int c = tcsetattr(STDIN_FILENO,TCSANOW, &t_restore);
    if (c != 0)
    {
        smart_error("t attributes");
        
    }
}

void close_with_error(int fd)
{
    int i = close(fd);
    if(i < 0)
    {
        fprintf(stderr, "Close failed with: %s\n",strerror(errno));
        exit(1);
    }
    
}


void process_input(char * buffer, ssize_t bytes, int fd)
{
int i = 0;
    if(compressFlag == 0)
    {
        int sofar = 0;
        for(i = 0; i < bytes; i++)
        {
            switch(*(out+i))
        
            {
             
                    log_with_error(fd, tmp, 1);
                    write_with_error(fd, out, i-1);
                    log_with_error(fd, out, i-1,1);
                    
                    sofar+= i;
                    break;
               default:
                    if(i == bytes -1)
                    write_with_error(fd, out+sofar, i - sofar);
                    log_with_error(fd, out+sofar, i - sofar, 1);
                    
            }
        }
       
    
    }
    // compress
    
    else if(compressFlag == 1 && fd != STDOUT_FILENO)
    {
        for(i = 0; i < bytes; i++)
        {
            switch(*(buffer+i))
            
            {
                case '\r':
                case '\n':
                {
                        buffer[i] = '\n';
                }
                    break;
                default:
                    ;
                    
            }
        }
        strm.zalloc = Z_NULL;
        strm.zfree = Z_NULL;
        strm.opaque = Z_NULL;
        
        deflateInit(&strm, Z_DEFAULT_COMPRESSION);
        deflate(&strm, Z_SYNC_FLUSH);
        deflateEnd(&strm);
        
        strm.avail_in = bytes;
        strm.next_in = buffer;
        strm.avail_out = CHUNK;
        strm.next_out = out;
       
        write_with_error(fd,out, strm.total_out);
        log_with_error(fd, out, strm.total_out, 1);

        
        memset(&strm, 0, sizeof strm);
        memset(&buffer, 0, sizeof buffer);
        memset(&out, 0, sizeof out);

        
    }
    else if (compressFlag == 1 && fd == STDOUT_FILENO)
    {
        
        strm_server.zalloc = Z_NULL;
        strm_server.zfree = Z_NULL;
        strm_server.opaque = Z_NULL;
        
        strm_server.avail_in = bytes;
        strm_server.next_in = buffer;
        strm_server.avail_out = CHUNK;
        strm_server.next_out = out;
        
        inflateInit(&strm_server);
        inflate(&strm_server, Z_SYNC_FLUSH);
        inflateEnd(&strm_server);
        
        log_with_error(fd, out, strm_server.total_out, 0);
        write_with_error(fd, out, strm_server.total_out);
        

        
        memset(&strm_server, 0, sizeof strm_server);
        memset(&buffer, 0, sizeof buffer);
        memset(&out, 0, sizeof out);
        
    }
    else
    {
        ;
    }
    
    
    
    
    
}



int main(int argc, char **argv)
{
    int c;
    // debugFlag = 1;
    int portno;
    // get options 
    while (1) {
        static struct option long_options[] = {
            {"compress",    no_argument, 0,  'c'},
            {"log", required_argument, 0, 'l'},
            {0,         0,                 0,  0 }
        };
        c = getopt_long(argc, argv, "",
                        long_options, NULL);
        if (c == -1)
            break;
        else if (c == 'c')
        {
           	compressFlag = 1;
        }
        else if (c == 'l')
        {
            logFlag = 1;
	    strncpy(logFile, optarg, 256);
        }
        else if (c== 'p')
        {
            portno = atoi(optarg);
        }
       	else if( c== 'd')
       	{
            debugFlag =1;
       	}
        else if (c == '?')
        {
            fprintf(stderr, "one or more options is unrecognized. \n");
            exit(1);
        }
        else
            continue;
    }
    
    struct termios t_options;
    tcgetattr(STDIN_FILENO, &t_restore);
    tcgetattr(STDIN_FILENO, &t_options);
    t_options.c_lflag = 0;
    t_options.c_oflag = 0;
    t_options.c_iflag = ISTRIP;
    
    int  j = tcsetattr(STDIN_FILENO, TCSANOW, &t_options);
    if (j == -1)
        {
            smart_error("t set attributes");
        }
    
    int sockfd, n;
    struct sockaddr_in serv_addr;
    struct hostent *server;
    
    
    memset((char *) &serv_addr,0, sizeof(serv_addr));
    server = gethostbyname("localhost");
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if(sockfd < 0)
   {
        smart_error("SOCKET");
    }
    printf("created socket\n");
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(portno);
    memcpy((char *) &serv_addr.sin_addr.s_addr, (char *) server->h_addr, server->h_length);
    if (connect(sockfd,(struct sockaddr *)&serv_addr,sizeof(serv_addr)) < 0)
    {
        smart_error("CONNECT");
    }
    else
      {
	printf("connect called success\n");
      }

    
   

 
    
    struct pollfd pole[1];
    pole[0].fd = sockfd;
    pole[0].events = POLLIN | POLLHUP | POLLERR;
    
    
    while(1)
      {
	printf("polling");
        int j = poll(pole, 2, 0);
        if (j == -1)
        {
            smart_error("Polling");
        }
        else
        {
            if (j ==0)
                continue;
            if(pole[0].revents & POLLIN)
            {
                //read input from the keyboard, echo out and forward
                char key_in[256];
                ssize_t bytes_read;
                bytes_read = read_with_error(0, key_in, 256);
                write_with_error(STDOUT_FILENO, key_in, bytes_read);
		printf("writing\n");
                process_input(key_in, bytes_read,pole[1].fd);
            }
            if(pole[1].revents & POLLIN)
            {
                // input from server
                char server_in[256];
                ssize_t bytes_read = read_with_error(pole[1].fd, server_in, 256);   
		printf("recv input from server\n");
                process_input(server_in, bytes_read, STDOUT_FILENO);
            }
            if(pole[1].revents & (POLLHUP | POLLERR))
            {
                close(pole[1].fd);
                exit(0);
            }
        }
    }
        
        
        
    }
    
