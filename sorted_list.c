//
//  SortedList.c
//
//
//  Created by Michael Kliger on 5/6/18.
//  Copyright © 2018 Michael Kliger. All rights reserved.
//

#include "SortedList.h"
#include <stddef.h>
#include <string.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sched.h>


void SortedList_insert(SortedList_t *list, SortedListElement_t *element)
{
  if(list == NULL || element == NULL)
    return;
  
  
    struct SortedListElement * node = list->next;
        
    
        while(node != list  && strcmp(node->key, element->key) < 0)
        {
	  node = node->next;
        }
            
        
    
    if(opt_yield & INSERT_YIELD)
      sched_yield();
     node->prev->next = element;
     element->prev = node->prev;
      element->next = node;
      node->prev = element;
        
    
    
}

int SortedList_delete( SortedListElement_t *element)
{
    if(element == NULL || element->next->prev != element->prev->next)
        return 1;
    
   
    
    
    if(opt_yield & DELETE_YIELD)
      sched_yield();
    element->prev->next = element->next;
    element->next->prev = element->prev;
   
  
    
    return 0;
}

SortedListElement_t *SortedList_lookup(SortedList_t *list, const char *key)
{
    if(list == NULL || key == NULL)
        return NULL;
    
    struct SortedListElement * node = list->next;
    
    while(node != list)
    {
        
        if(strcmp(node->key, key) > 0) // violation of ascending order
        {
            return NULL;
        }
        else if(strcmp(node->key, key) == 0)
            return node;
        else
        {
            if(opt_yield & LOOKUP_YIELD)
                sched_yield();
            node = node->next;
        }

        
    }
    return NULL;
}

int SortedList_length(SortedList_t *list)
{
  if(list == NULL)
    return -1;

    int count = 0;
    
    struct SortedListElement * node = list->next;
    
    while(node != list)
    {
        if(node->next->prev != node || node->prev->next != node)
          {
	    	    return -1;}

        else
        {
            count++;
            if(opt_yield & LOOKUP_YIELD)
                sched_yield();
            node = node->next;
           
        }
   
    }

    return count;
}